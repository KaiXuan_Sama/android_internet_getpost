package com.app.internet_getpost;

import androidx.appcompat.app.AppCompatActivity;
import okhttp3.Call;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.time.Instant;

public class MainActivity extends AppCompatActivity {
    Button bt_Get;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bt_Get = findViewById(R.id.bt_Get);

        bt_Get.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,okhttpActivity.class);
                startActivity(intent);
            }
        });
    }
}