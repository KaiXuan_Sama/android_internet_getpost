package com.app.internet_getpost;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.io.IOException;
import java.util.Objects;

public class okhttpActivity extends AppCompatActivity implements View.OnClickListener {
    private static final int GET = 1;
    private static final int POST = 2;

    Button bt_Get,bt_Post;
    TextView tv_show;

    OkHttpClient client = new OkHttpClient();

    /*UI处理线程*/
    private Handler handler = new Handler(){


        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case GET:
                    // 获取数据
                    tv_show.setText(msg.obj.toString());
                    break;
                case POST:
                    // 获取数据
                    tv_show.setText(msg.obj.toString());
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_okhttp);

        bt_Get = findViewById(R.id.bt_Get);
        bt_Post = findViewById(R.id.bt_Post);
        tv_show =findViewById(R.id.tv_show);

        // 设置点击事件
        bt_Get.setOnClickListener(this);
        bt_Post.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.bt_Get:       // 使用原生的okhttp请求网络数据，get
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String string = get("https://www.baidu.com");
                            Message msg = new Message();
                            msg.what = GET;
                            msg.obj = string;
                            handler.sendMessage(msg);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                break;
            case R.id.bt_Post:
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            String string = post("https://www.baidu.com","");
                            Message msg = new Message();
                            msg.what = POST;
                            msg.obj = string;
                            handler.sendMessage(msg);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }).start();
                break;
        }
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");    // 定义编码格式


    /**get okHttp的get请求
     * @param url 网络连接
     * @return
     */
    private String get(String url) throws IOException {
        Request request = new Request.Builder().url(url).build();
        Response response = client.newCall(request).execute();
        return Objects.requireNonNull(response.body()).string();
    }

    /**OkHttp的Post请求
     * @param url 网络链接
     * @param json 发送的json数据
     * @return
     * @throws IOException
     */
    private String post(String url,String json) throws IOException {
        RequestBody body = RequestBody.create(JSON,json);
        Request request =new Request.Builder().url(url).post(body).build();
        Response response = client.newCall(request).execute();
        return response.body().string();
    }
}